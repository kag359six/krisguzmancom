// @flow

import React from 'react';
import styled from 'styled-components/macro';
import NavigationLink from '../../atoms/Links';
import HamburgerButton from '../../atoms/Hamburger';
import { breakpoints } from '../../../theme';

const NavHeader = styled.div`
  width: 90%;
  margin: auto;
  height: ${props => props.theme.header.height.xl};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  color: ${props => props.theme.colors.secondary};

  @media ${breakpoints.lg} {
    height: ${props => props.theme.header.height.lg};
  }
`;

const HomeLink = styled(NavigationLink)`
  @media ${breakpoints.lg} {
    font-size: ${props => props.theme.fontSize.bodyText.lg};
  }
  @media ${breakpoints.md} {
    font-size: ${props => props.theme.fontSize.bodyText.md};
  }

  @media ${breakpoints.sm} {
    font-size: ${props => props.theme.fontSize.bodyText.sm};
  }

  @media ${breakpoints.xs} {
    font-size: ${props => props.theme.fontSize.bodyText.xs};
  }
`;

export default () => (
  <NavHeader>
    <HomeLink to="/">Kristopher Guzman</HomeLink>
    <HamburgerButton />
  </NavHeader>
);
