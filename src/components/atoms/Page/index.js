// @flow

import React from 'react';
import styled from 'styled-components/macro';

const PageContainer = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: ${props => props.theme.colors.background};
  overflow-x: hidden;
  overflow-y: auto;
`;

type PageProps = {
  children: any
};

export const Page = (props: PageProps) => (
  <PageContainer>{props.children}</PageContainer>
);

export default Page;
