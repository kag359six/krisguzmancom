// @flow

import React from 'react';
import { Link } from 'react-router-dom';
import { BodyText } from '../../typography';
import styled from 'styled-components';

type NavigationLinkProps = {
  to: string,
  className?: string,
  children: any
};

const NavigationLink = ({ className, to, children }: NavigationLinkProps) => (
  <div className={className}>
    <Link to={to}>
      <BodyText> {children} </BodyText>
    </Link>
  </div>
);

const StyledNavigationLink = styled(NavigationLink)`
  transition: opacity;
  transition-duration: 0.15s;
  &:hover {
    opacity: 0.7;
  }
`;

export default StyledNavigationLink;
