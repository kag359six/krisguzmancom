import React, { useState } from 'react';
import styled from 'styled-components/macro';
import { breakpoints } from '../../../theme';

type HamburgerButtonProps = {
  className?: string
};

const HamburgerButton = ({ className }: HamburgerButtonProps) => {
  const [isActive, setIsActive] = useState(false);
  return (
    <div className={className}>
      <button
        className={`hamburger hamburger--emphatic ${
          isActive ? 'is-active' : ''
        }`}
        type="button"
        aria-label="Menu"
        aria-controls="navigation"
        aria-expanded={isActive}
        onClick={() => setIsActive(!isActive)}
      >
        <span className="hamburger-box">
          <span className="hamburger-inner" />
        </span>
      </button>
    </div>
  );
};

export default styled(HamburgerButton)`
  transform: scale(1.4);
  @media ${breakpoints.lg} {
    transform: scale(1);
  }
  @media ${breakpoints.xs} {
    transform: scale(0.8);
  }
`;
