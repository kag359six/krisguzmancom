// @flow

import React from 'react';
import styled from 'styled-components/macro';
import type { StyledComponent } from '../../theme';
import { breakpoints } from '../../theme';

export const Color: StyledComponent = styled.span`
  color: ${props => props.theme.colors[props.color]};
`;

export const BodyText: StyledComponent = styled.p`
  display: inline;
  margin: 0;
  font-size: ${props => props.theme.fontSize.bodyText.xl};
  font-weight: ${props => (props.bold ? '500' : '300')};
  font-family: 'Prata';
  @media ${breakpoints.lg} {
    font-size: ${props => props.theme.fontSize.bodyText.lg};
  }
  @media ${breakpoints.md} {
    font-size: ${props => props.theme.fontSize.bodyText.md};
  }

  @media ${breakpoints.sm} {
    font-size: ${props => props.theme.fontSize.bodyText.sm};
  }

  @media ${breakpoints.xs} {
    font-size: ${props => props.theme.fontSize.bodyText.xs};
  }
`;

export const Header1: StyledComponent = styled.h1`
  color: ${props =>
    props.color
      ? props.theme.colors[props.color]
      : props.theme.colors.dullGold};
  font-size: ${props => props.theme.fontSize.header1.xl};
  margin: 0;
  padding: 0;
  @media ${breakpoints.lg} {
    font-size: ${props => props.theme.fontSize.header1.lg};
  }
  @media ${breakpoints.md} {
    font-size: ${props => props.theme.fontSize.header1.md};
  }

  @media ${breakpoints.sm} {
    font-size: ${props => props.theme.fontSize.header1.sm};
  }

  @media ${breakpoints.xs} {
    font-size: ${props => props.theme.fontSize.header1.xs};
  }
`;

type LandingPageTextProps = {
  primaryText: string,
  secondaryText: string,
  className?: String
};

const LandingPageTextComp = ({
  primaryText,
  secondaryText,
  className
}: LandingPageTextProps) => (
  <div className={className}>
    <Header1>
      <Color color="primary">{primaryText}</Color> <br />
      <Color color="secondary">{secondaryText}</Color>
    </Header1>
  </div>
);

export const LandingPageText = styled(LandingPageTextComp)`
  width: 1100px;
  @media ${breakpoints.lg} {
    width: 800px;
  }
  @media ${breakpoints.md} {
    width: 700px;
    margin-left: 25px;
  }

  @media ${breakpoints.sm} {
    width: 600px;
    margin-left: 0;
  }

  @media ${breakpoints.xs} {
    width: 375px;
  }
`;
