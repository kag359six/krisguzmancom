// @flow

import React from 'react';
import styled from 'styled-components/macro';
import { LandingPageText } from '../../components/typography';
import Page from '../../components/atoms/Page';
import Header from '../../components/molecules/Header';
import { breakpoints } from '../../theme';
import {
  LANDING_PAGE_COPY_1_PRIMARY,
  LANDING_PAGE_COPY_1_SECONDARY
} from '../../const/copy';

const ContentWrapper = styled.div`
  margin: auto;
  width: 90%;
  height: calc(100% - ${props => props.theme.header.height.xl});
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media ${breakpoints.lg} {
    height: calc(100% - ${props => props.theme.header.height.lg});
  }
`;

export const LandingPage = () => {
  return (
    <Page>
      <Header />
      <ContentWrapper>
        <LandingPageText
          primaryText={LANDING_PAGE_COPY_1_PRIMARY}
          secondaryText={LANDING_PAGE_COPY_1_SECONDARY}
        />
      </ContentWrapper>
    </Page>
  );
};

export default LandingPage;
