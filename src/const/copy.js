export const LANDING_PAGE_COPY_1_PRIMARY = 'Problems are everywhere.';
export const LANDING_PAGE_COPY_1_SECONDARY =
  'I help break them down technically.';
