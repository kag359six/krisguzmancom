import { createGlobalStyle } from 'styled-components/macro';

export default createGlobalStyle`

  html {
    font-size: 10px;
  }

  body {
    margin: 0;
    padding: 0;
  }
  #root {
    margin: 0;
    padding: 0;
    overflow-x: hidden;
  }

  button:focus {
    outline: none;
    border: none;
    background: none;
  }

  a:link    {
    /* Applies to all unvisited links */
    text-decoration:  none;
    background: none;
    } 
  a:visited {
    /* Applies to all visited links */
    color: inherit;
    text-decoration:  none;
    background: none;
    } 
  a:hover   {
    /* Applies to links under the pointer */
    text-decoration:  none;
    background: none;
    } 
  a:active  {
    /* Applies to activated links */
    text-decoration:  none;
    background: none;
    } 

  h1, h2, h3 {
    font-family: '${props => props.theme.fontFamily.header}';
    font-weight: normal;
    font-style: normal;
  }

   /*!
* Credit to Jonathan Suh for providing a bunch of css hamburger buttons!
* @author Jonathan Suh @jonsuh
* @site https://jonsuh.com/hamburgers
* @link https://github.com/jonsuh/hamburgers
*/

  .hamburger {
    display: inline-block;
    cursor: pointer;
    transition-property: opacity, filter;
    transition-duration: 0.15s;
    transition-timing-function: linear;
    font: inherit;
    color: inherit;
    text-transform: none;
    background-color: transparent;
    border: 0;
    margin: 0;
    overflow: visible;
  }
  .hamburger:hover {
    opacity: 0.7;
  }
  .hamburger.is-active:hover {
    opacity: 0.7;
  }
  .hamburger.is-active .hamburger-inner,
  .hamburger.is-active .hamburger-inner::before,
  .hamburger.is-active .hamburger-inner::after {
    background-color: ${props => props.theme.colors.secondary};
  }

  .hamburger--emphatic {
    overflow: hidden;
  }
  .hamburger--emphatic .hamburger-inner {
    transition: background-color 0.3s 0.175s ease-in;
  }
  .hamburger--emphatic .hamburger-inner::before {
    left: 0;
    transition: transform 0.3s cubic-bezier(0.6, 0.04, 0.98, 0.335),
      top 0.05s 0.3s linear, left 0.3s 0.175s ease-in;
  }
  .hamburger--emphatic .hamburger-inner::after {
    top: 10px;
    right: 0;
    transition: transform 0.3s cubic-bezier(0.6, 0.04, 0.98, 0.335),
      top 0.05s 0.3s linear, right 0.3s 0.175s ease-in;
  }
  .hamburger--emphatic.is-active .hamburger-inner {
    transition-delay: 0s;
    transition-timing-function: ease-out;
    background-color: transparent !important;
  }
  .hamburger--emphatic.is-active .hamburger-inner::before {
    left: -80px;
    top: -80px;
    transform: translate3d(80px, 80px, 0) rotate(45deg);
    transition: left 0.3s ease-out, top 0.05s 0.3s linear,
      transform 0.3s 0.175s cubic-bezier(0.075, 0.82, 0.165, 1);
  }
  .hamburger--emphatic.is-active .hamburger-inner::after {
    right: -80px;
    top: -80px;
    transform: translate3d(-80px, 80px, 0) rotate(-45deg);
    transition: right 0.3s ease-out, top 0.05s 0.3s linear,
      transform 0.3s 0.175s cubic-bezier(0.075, 0.82, 0.165, 1);
  }
  .hamburger-box {
    width: 40px;
    height: 24px;
    display: inline-block;
    position: relative;
  }
  .hamburger-inner {
    display: block;
    top: 50%;
    margin-top: -2px;
  }
  .hamburger-inner,
  .hamburger-inner::before,
  .hamburger-inner::after {
    width: 40px;
    height: 4px;
    background-color: ${props => props.theme.colors.secondary};
    border-radius: 4px;
    position: absolute;
    transition-property: transform;
    transition-duration: 0.15s;
    transition-timing-function: ease;
  }
  .hamburger-inner::before,
  .hamburger-inner::after {
    content: '';
    display: block;
  }
  .hamburger-inner::before {
    top: -10px;
  }
  .hamburger-inner::after {
    bottom: -10px;
  }
`;
