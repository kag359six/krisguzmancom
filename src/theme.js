// @flow

import type { ComponentType } from 'react';

type Breakpoints = {
  lg: '(max-width: 1440px)',
  md: '(max-width: 1024px)',
  sm: '(max-width: 768px)',
  xs: '(max-width: 528px)'
};

export const breakpoints: Breakpoints = {
  lg: '(max-width: 1440px)',
  md: '(max-width: 1024px)',
  sm: '(max-width: 768px)',
  xs: '(max-width: 528px)'
};

const header = Object.freeze({
  height: {
    xl: '150px',
    lg: '125px'
  }
});

const fontSize = Object.freeze({
  header1: {
    xl: '86px',
    lg: '64px',
    md: '50px',
    sm: '45px',
    xs: '38px'
  },
  header2: '150px',
  header3: '1200px',
  bodyText: {
    xl: '28px',
    lg: '21px',
    md: '21px',
    sm: '18px',
    xs: '18px'
  },
  blurbText: {
    lg: '1.5vw',
    md: '2.5vw',
    sm: '3vw',
    xs: '3.5vw'
  },
  navLinks: {
    md: '1.5vw',
    lg: '1.2vw'
  },
  homeLinkText: {
    lg: '1.6vw',
    md: '2vw'
  }
});

//frozen objects make $Values see these as string literals rather than generic strings
const colors = Object.freeze({
  primary: '#2F2F2F',
  secondary: '#939393',
  tertiary: '#EBEAEA',
  disabled: '#D4D4D4',
  background: '#FFFFFF'
});

const fontFamily = {
  header: 'Prata',
  bodyText: 'Prata'
};

export type Colors = typeof colors;

type Theme = {
  colors: Colors,
  fontSize: typeof fontSize,
  fontFamily: typeof fontFamily,
  header: typeof header
};

type OverridableStyleProps = {
  margin?: string,
  bold?: boolean,
  fontSize?: string,
  fontWeight?: '300' | '700',
  color?: $Keys<Colors>
};

export type StyledComponent = ComponentType<OverridableStyleProps>;

const theme: Theme = {
  fontSize,
  fontFamily,
  colors,
  header
};

export default theme;
