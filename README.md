## Portfolio Website

The purpose of this website is to not only display my work, but to also give you a glimpse of how I work.
I'm using Create React App as a base to save me from all the boilerplate configurations. I added custom
configurations, libraries, and plugins on top to adjust to my workflow.
